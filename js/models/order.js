var Order = DS.Model.extend({
	name: 						DS.attr(),
	email: 		  			DS.attr(),
	telephone: 				DS.attr(),
	address: 					DS.attr(),
	postal: 					DS.attr(),
	longitude: 			  DS.attr(),
	latitude: 			  DS.attr(),
	service_option:   DS.attr(),
	pickup_date: 			DS.attr(),
	delivery_date: 		DS.attr(),
	pickup_time: 			DS.attr(),
	delivery_time: 		DS.attr(),
	promo_code: 			DS.attr(),
	notes: 						DS.attr(),
	billing: 					DS.attr(),
	user_id: 					DS.attr(),
	status: 					DS.attr(),
	sequence: 				DS.attr(),
	up_next: 					DS.attr(),
	done:  						DS.attr(),
	orders: 					DS.attr()
});


Order.FIXTURES = [
	{
		sequence: 1,
		orders: [
			{	
				name: "Alexandria Bryant",
				email: "alex@bryant.com",
				telephone: "5148872837",
				address: "1695 Saint Patrick #403 (12)",
				postal: "h3s2c4",
				longitude: "-73.56246",
				latitude: "45.48579",
				pickup_date: "Nov 15, 2013",
				delivery_date: "Nov 18, 2013",
				pickup_time: "18:00 - 21:00",
				delivery_time: "18:00 - 21:00",
				promo_code: {},
				notes: "",
				billing: "4*******3223",
				user_id: "1897",
				status: "pending pickup"
			},
			{
				name: "Henry Ford",
				email: "alex@bryant.com",
				telephone: "5148872837",
				address: "1695 Saint Patrick #403 (12)",
				postal: "h3s2c4",
				longitude: "-73.56246",
				latitude: "45.48579",
				pickup_date: "Nov 15, 2013",
				delivery_date: "Nov 18, 2013",
				pickup_time: "18:00 - 21:00",
				delivery_time: "18:00 - 21:00",
				promo_code: {},
				notes: "",
				billing: "4*******3223",
				user_id: "1897",
				status: "pending pickup"	
			}
		],
		done: false,
		up_next: false
	},
	{
		sequence: 2,
		orders:[
			{
				name: "Michael Jackson",
				email: "alex@bryant.com",
				telephone: "5148872837",
				address: "1695 Saint Patrick #403 (12)",
				postal: "h3s2c4",
				longitude: "-73.56246",
				latitude: "45.48579",
				pickup_date: "Nov 15, 2013",
				delivery_date: "Nov 18, 2013",
				pickup_time: "18:00 - 21:00",
				delivery_time: "18:00 - 21:00",
				promo_code: {},
				notes: "",
				billing: "4*******3223",
				user_id: "1897",
				status: "pending pickup"
			}
		],
		done: false,
		up_next: false
	},
	{
		sequence: 3,
		orders: [
			{
				name: "Kobe Bryant",
				email: "alex@bryant.com",
				telephone: "5148872837",
				address: "1695 Saint Patrick #403 (12)",
				postal: "h3s2c4",
				longitude: "-73.56246",
				latitude: "45.48579",
				pickup_date: "Nov 15, 2013",
				delivery_date: "Nov 18, 2013",
				pickup_time: "18:00 - 21:00",
				delivery_time: "18:00 - 21:00",
				promo_code: {},
				notes: "",
				billing: "4*******3223",
				user_id: "1897",
				status: "pending pickup"
			}
		],
		done: false,
		up_next: false
	},
	{
		sequence: 4,
		orders: [
			{
				name: "Alexandria Bryant",
				email: "alex@bryant.com",
				telephone: "5148872837",
				address: "1695 Saint Patrick #403 (12)",
				postal: "h3s2c4",
				longitude: "-73.56246",
				latitude: "45.48579",
				pickup_date: "Nov 15, 2013",
				delivery_date: "Nov 18, 2013",
				pickup_time: "18:00 - 21:00",
				delivery_time: "18:00 - 21:00",
				promo_code: {},
				notes: "",
				billing: "4*******3223",
				user_id: "1897",
				status: "pending pickup"
			}
		],
		done: false,
		up_next: false
	}
]

module.exports = Order;



