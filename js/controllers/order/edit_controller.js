var OrderEditController = Ember.ObjectController.extend({
	actions: {
		save: function() {
			var order = this.get('model');
			order.save();

			this.transitionToRoute('order', order);
		}
	}
});

module.exports = OrderEditController;

