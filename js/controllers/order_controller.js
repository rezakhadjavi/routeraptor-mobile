var OrderController = Ember.ObjectController.extend({
	showComingUp: function(){
		
		var done = this.get('done');
		var up_next = this.get('up_next')

		if (done == false){
			if(up_next == false) {
				return true;
			}
		}

	}.property('done', 'up_next'),

	multiple: function(){
		var orders = this.get('orders');
		if (orders.length > 1) {
			return true;
		}
		
	}.property('orders'),

	actions: {
		edit: function(){
			this.transitionToRoute('order.edit');
		},
		delivered: function(){
			// orders are sorted by "sequence" attr.
			var current_sequence = this.get('sequence');

			// find the next order in pipeline
			var next_sequence = current_sequence+=1;

			console.log(next_sequence);

			var that = this;

			this.get("store").find("order", {sequence: next_sequence}).then(function(order){
				var next_order = order.get("firstObject");

				next_order.set("up_next", true);
				next_id = next_order.get("id");
				that.replaceWith("order", next_id);
				// that.transitionToRoute("order", next_id);
			});

			this.toggleProperty('up_next');

			this.toggleProperty('done');

		}
	}
	
});

module.exports = OrderController;