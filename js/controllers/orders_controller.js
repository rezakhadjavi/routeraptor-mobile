var OrdersController = Ember.ArrayController.extend({
	itemController: 'order',
	sortProperties: ['sequence'],
	sortAscending: true
	// multipleSequence: function() {
	// 	var content = this.get("content") || [];
	// 	return Ember.ArrayProxy.create({
	// 		content: content.toArray()
	// 	})
	// }.property("content.@each")
});

module.exports = OrdersController;

