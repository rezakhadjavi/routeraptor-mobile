var App = require('./app');

App.Router.map(function() {
	this.resource("orders", function(){
		this.resource('order', { path: '/:order_sequence'}, function(){
			this.route('edit');
		})
	});
});

