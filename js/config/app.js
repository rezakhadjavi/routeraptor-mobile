// require other, dependencies here, ie:
// require('./vendor/moment');

require('../vendor/jquery');
require('../vendor/handlebars');
require('../vendor/ember');
require('../vendor/ember-data'); // delete if you don't want ember-data


var App = Ember.Application.create();
// App.Store = require('./store'); // delete if you don't want ember-data

App.ApplicationAdapter = DS.FixtureAdapter.extend({
	queryFixtures: function(fixtures, query, type) {
    console.log(query);
    console.log(type);
    return fixtures.filter(function(item) {
      for(prop in query) {
        if( item[prop] != query[prop]) {
            return false;
        }
      }
      return true;
    });
  },
  fixturesForType: function(type) {
  if (type.FIXTURES) {
   var fixtures = Ember.A(type.FIXTURES);
   return fixtures.map(function(fixture){

    // aka we massasge the data a bit here so the fixture adapter won't whine so much
    fixture.id = fixture.sequence;
    var fixtureIdType = typeof fixture.id;
    if(fixtureIdType !== "number" && fixtureIdType !== "string"){
      throw new Error(fmt('the id property must be defined as a number or string for fixture %@', [fixture]));
    }
    fixture.id = fixture.id + '';
    return fixture;
   });
  }
  return null;
 },
});




module.exports = App;

